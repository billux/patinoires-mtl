# patinoire.py

`patinoire.py` lists and shows conditions of skatings rinks in Montreal (QC), Canada.

## Usage

List open skating rinks:

```
$ ./patinoire.py list --open
Skating rink                                          Open
----------------------------------------------------  ----
Patinoire Bleu Blanc Bouge du parc Hayward (PSE)      1
Patinoire Bleu-Blanc-Bouge, Parc Confédération (PSE)  1
Patinoire réfrigérée, Lac aux Castors (PP)            1
```

List skating rinks in Plateau-Mont-Royal:

```
$ ./patinoire.py list --borough pmr
Skating rink                                           Open
-----------------------------------------------------  ----
Grande patinoire avec bandes, La Fontaine (PSE)        0
Patinoire avec bandes, Baldwin (PSE)                   0
Patinoire avec bandes, Jeanne-Mance (PSE)              0
Patinoire avec bandes, Sir-Wilfrid-Laurier no 1 (PSE)  0
Patinoire avec bandes, Sir-Wilfrid-Laurier no 2 (PSE)  0
Patinoire avec bandes, Sir-Wilfrid-Laurier no 3 (PSE)  0
Patinoire De Gaspé/Bernard (PSE)                       0
Patinoire de patin libre, Baldwin (PPL)                0
Patinoire de patin libre, Jeanne-Mance (PPL)           0
Patinoire de patin libre, Sir-Wilfrid-Laurier (PPL)    0
Patinoire décorative, De Lorimier (PP)                 0
Patinoire décorative, La Fontaine (PP)                 0
```

To list all boroughs and borough codes:

```
$ ./patinoire.py list-boroughs
Key  Name
---  --------------------------------------
cdn  Côte-des-Neiges - Notre-Dame-de-Grâce
lsl  LaSalle
pmr  Le Plateau-Mont-Royal
lch  Lachine
vsp  Villeray-Saint-Michel - Parc-Extension
vma  Ville-Marie
```

Show conditions of a specific skating rink:

```
$ ./patinoire.py conditions "Patinoire réfrigérée, Lac aux Castors (PP)"
                                            Open  Cleared  Sprayed  Resurfaced  Condition   
------------------------------------------  ----  -------  -------  ----------  ----------  
Patinoire réfrigérée, Lac aux Castors (PP)  1     1        1        1           Excellente 
```

Or by substring match (case insensitive):

```
$ ./patinoire.py conditions "lac aux castors"
                                            Open  Cleared  Sprayed  Resurfaced  Condition   
------------------------------------------  ----  -------  -------  ----------  ----------  
Patinoire réfrigérée, Lac aux Castors (PP)  1     1        1        1           Excellente 
```

Watch for conditions changes at a specific skating rink. The command will not return anything until conditions will get updated:

```
$ ./patinoire.py watch "Patinoire réfrigérée, Lac aux Castors (PP)"
                                            Open  Cleared  Sprayed  Resurfaced  Condition
------------------------------------------  ----  -------  -------  ----------  ----------
Patinoire réfrigérée, Lac aux Castors (PP)  1     1        1        1           Excellente
Updated on 2018-12-08 09:05:28
$ ./patinoire.py watch "Patinoire réfrigérée, Lac aux Castors (PP)"
$
[a few days later]
$ ./patinoire.py watch "Patinoire réfrigérée, Lac aux Castors (PP)"
                                            Open  Cleared  Sprayed  Resurfaced  Condition
------------------------------------------  ----  -------  -------  ----------  ----------
Patinoire réfrigérée, Lac aux Castors (PP)  1     1        1        1           Bonne
Updated on 2018-12-10 09:03:56
```

You can use `patinoire.py watch` with cron and pipe output to `mail`, `sendxmpp`, or anything else to get notified for new conditions.

Enjoy the winter!

## Credits

  * This script is licensed under the terms of the GNU General Public License version 3. Copyright Romain Dessort.
  * The data related to skating rinks are licensed under the terms of the Creative Commons Attribution 4.0 International license. Copyright [Ville de Montréal](http://donnees.ville.montreal.qc.ca/dataset/patinoires)
