#!/usr/bin/env python3
#
# Copyright (C) 2018  Romain Dessort <romain@univers-libre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
patinoire.py lists and show conditions of skatings rinks in Montreal. Data are
fetched from Montreal's open data website.
"""

import argparse
import urllib.request
import xml.etree.ElementTree as ET
import os.path

XML_RESOURCE = 'http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml'
TMPDIR = '/tmp/'

def fetch_xml():
    """
    Fetch XML data from Montreal city's website.
    """
    response = urllib.request.urlopen(XML_RESOURCE).read()
    return ET.fromstring(response)


def print_table(table, table_titles=[]):

    # Transpose table's rows and columns and get the longest element of each
    # column for each row so we can align columns with the right number of
    # spaces.
    row_format = ''
    col_width = []
    for line in list(zip(*list(table)+[table_titles])):
        width = max([len(i) for i in line])
        row_format = row_format + "{:<" + str(width + 2) + '}'
        col_width.append(width)

    # Print table title and separator.
    if len(table_titles) > 0:
        print(row_format.format(*table_titles))
        print(row_format.format(*['-' * width for width in col_width]))

    # Print each line of the table.
    for line in table:
        print(row_format.format(*line))


def list_boroughs(xml_data, args):
    """
    Print a list of all borough found in the XML, with their key.
    """

    boroughs = {}
    for skating_rink in xml_data.findall('patinoire'):
        borough_key = skating_rink.find('arrondissement').find('cle').text
        borough_name = skating_rink.find('arrondissement').find('nom_arr').text
        boroughs[borough_key] = borough_name

    print_table(boroughs.items(), ['Key', 'Name'])


def list_skating_rinks(xml_data, args):
    """
    Print a list of skating rinks found in the XML based on some conditions
    pased as arguments.
    """

    skating_rink_table = []
    for skating_rink in xml_data.findall('patinoire'):

        if args.open:
            # tag "ouvert" isn't always == 0 if skating rink is closed.
            if skating_rink.find('ouvert').text != '1':
                continue

        if args.borough:
            if skating_rink.find('arrondissement').find('cle').text != args.borough:
                continue

        skating_rink_table.append([skating_rink.find('nom').text, skating_rink.find('ouvert').text or '0'])

    print_table(skating_rink_table, ['Skating rink', 'Open'])


def show_conditions(xml_data, args):
    """
    Print current conditions of a given skating rink.
    """

    for skating_rink in xml_data.findall('patinoire'):

        if  args.skatingrink.lower() in skating_rink.find('nom').text.lower():
            # These tags sometimes don't contain any text in them, so we
            # default them to 0.
            print_table([[skating_rink.find('nom').text, skating_rink.find('ouvert').text or '0',
                skating_rink.find('deblaye').text or '0', skating_rink.find('arrose').text or '0',
                skating_rink.find('resurface').text or '0', skating_rink.find('condition').text or '0']],
                ['', 'Open','Cleared','Sprayed','Resurfaced','Condition'])
            print('Updated on ' + skating_rink.find('arrondissement').find('date_maj').text)


def watch(xml_data, args):
    """
    Watch a skating rink for update on its conditions. Print conditions only if
    they changed after the last run.
    """

    for skating_rink in xml_data.findall('patinoire'):

        if skating_rink.find('nom').text == args.skatingrink:
            filename = os.path.join(TMPDIR, skating_rink.find('nom').text.replace(' ', '_') + '.xml')
            try:
                skating_rink_old = ET.parse(filename)
            # First call of watch(), the file doesn't exist yet so we just
            # print the conditions and save the XML subtree to the file.
            except FileNotFoundError:
                show_conditions(xml_data, args)
                skating_rink_tree = ET.ElementTree(skating_rink)
                skating_rink_tree.write(filename)
            else:
                if skating_rink.find('arrondissement').find('date_maj').text \
                        != skating_rink_old.find('arrondissement').find('date_maj').text:
                    if skating_rink.find('ouvert') != skating_rink_old.find('ouvert') \
                            or skating_rink.find('deblaye') != skating_rink_old.find('deblaye') \
                            or skating_rink.find('arrose') != skating_rink_old.find('arrose') \
                            or skating_rink.find('resurface') != skating_rink_old.find('resurface'):
                        show_conditions(xml_data, args)
                        skating_rink_tree = ET.ElementTree(skating_rink)
                        skating_rink_tree.write(filename)

            break


if __name__ == '__main__':

    # Parse arguments.
    parser = argparse.ArgumentParser(
        description='patinoire.py lists and shows conditions of skatings rinks in Montreal')
    subparsers = parser.add_subparsers(dest='command', help='Available commands')

    parser_boroughs = subparsers.add_parser('list-boroughs', help='List boroughs')
    parser_boroughs.set_defaults(func=list_boroughs)

    parser_list = subparsers.add_parser('list', help='List skating rinks')
    parser_list.add_argument('--open', action='store_true',
                             help='Show only open skating rinks')
    parser_list.add_argument('--borough', metavar='BOROUGH_CODE',
                             help='Show only skating rinks from borough BOROUGH_CODE')
    parser_list.set_defaults(func=list_skating_rinks)

    parser_cond = subparsers.add_parser('conditions',
                                        help='Show current conditions for skating rink(s)')
    parser_cond.add_argument('skatingrink',
                                        help='Skating rink name. It could be exact name or a substring of name (case insensitive)')
    parser_cond.set_defaults(func=show_conditions)

    parser_watch = subparsers.add_parser('watch',
                                         help='Watch conditions for a skating rink')
    parser_watch.add_argument('skatingrink', help='Skating rink')
    parser_watch.set_defaults(func=watch)

    args = parser.parse_args()
    if args.command:
        xml_data = fetch_xml()
        args.func(xml_data, args)
    else:
        parser.print_help()
